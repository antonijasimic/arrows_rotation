$(document).ready(function(){
  $(".btn-arrow").on("mouseover",function(){
    var btn = $(this);
    var fa = $(btn).find(".fa");
    $(fa).addClass("rotateIcon").removeClass("unRotateIcon");
  });

  $(".btn-arrow").on("mouseout",function(){
   var btn = $(this);
   var fa = $(btn).find(".fa");
   $(fa).addClass("unRotateIcon").removeClass("rotateIcon");
 });
});